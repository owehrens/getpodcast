import datetime

from pytest import raises

from getpodcast import (
    getSafeFilenameFromText,
    parseFileExtensionFromUrl,
    parseRftTimeToDatetime,
    parseUnixTimeToDatetime,
)


def test_parse_rft_time_to_datetime():
    fn = parseRftTimeToDatetime
    with raises((TypeError, ValueError)):
        fn("")
    rft_date = "Mon, 20 Nov 1995 19:12:08 -0000"
    assert isinstance(fn(rft_date), datetime.datetime)
    assert fn(rft_date) == datetime.datetime(1995, 11, 20, 19, 12, 8)


def test_parse_unix_time_to_datetime():
    fn = parseUnixTimeToDatetime
    with raises(TypeError):
        fn("")
    assert isinstance(fn(0), datetime.datetime)
    assert fn(0) == datetime.datetime(1970, 1, 1)


def test_get_safe_filename_from_text():
    fn = getSafeFilenameFromText
    assert isinstance(fn(""), str)
    assert fn("") == ""
    assert fn("Episode 1") == "Episode 1"
    assert fn("Episode:**{=}1") == "Episode=1"
    assert fn("Episode 1 > COM1") == "Episode 1  "
    assert fn("echo $USER >>") == "echo USER "
    assert fn("echo $(pwd)") == "echo (pwd)"
    assert fn(r"c:\Windows\file.dll") == "cWindowsfile.dll"
    assert fn(r"/etc/file.conf") == "etcfile.conf"


def test_parse_file_extension_from_url():
    fn = parseFileExtensionFromUrl
    with raises(TypeError):
        fn(0)
    assert isinstance(fn(""), str)
    assert fn("episode_1.ogg") == ".ogg"
    assert fn("episode_1.OGG") == ".ogg"
    assert fn("episode_1.ogg?uid=9119") == ".ogg"
    assert fn("dl.php?fn=episode_1.ogg") == ".ogg"
