#! /usr/bin/env python3

"""
You can customize the date-format by using ``{year}``, ``{month}`` and ``{day}``
instead of ``{date}``.
"""

import os

import getpodcast

opt = getpodcast.options(
    date_from="2021-01-01",
    user_agent="Mozilla/5.0",
    root_dir=os.path.expanduser("~/podcast"),
    template="{rootdir}/{podcast}/{year}/{year}-{month}-{day} - {title}{ext}",
)

podcasts = {
    "SGU": "https://feed.theskepticsguide.org/feed/sgu",
    "Taskmaster": "https://feed.podbean.com/taskmasterpodcast/feed.xml",
    "Neebscast": "https://www.blubrry.com/feeds/neebscast.xml",
}

getpodcast.getpodcast(podcasts, opt)
