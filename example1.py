#! /usr/bin/env python3

"""
A simple basic example:

* Download all episodes since 2021.
* Store the episodes in the ``./podcast`` folder.
"""

import getpodcast

opt = getpodcast.options(date_from="2021-01-01", root_dir="./podcast")

podcasts = {
    "SGU": "https://feed.theskepticsguide.org/feed/sgu",
    "Radiolab": "http://feeds.wnyc.org/radiolab",
    "The Ron Burgundy Podcast": "https://feeds.megaphone.fm/HSW7933892085",
    "Neebscast": "https://www.blubrry.com/feeds/neebscast.xml",
}

getpodcast.getpodcast(podcasts, opt)
