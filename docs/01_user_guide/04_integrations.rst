============
Integrations
============

Cronjob
=======

Setup cronjob to download once a day:

.. code-block:: console

    $ crontab -e

    0 19 * * * /usr/bin/python3 /home/myuser/mypodcasts.py --quiet --onlynew --run


A simple command that lists downloaded episodes for the last 10 days.

.. code-block:: console
    
    $ crontab -e

    0 20 * * * find ./podcast/ -type f -mtime -10 > ./latest.txt
