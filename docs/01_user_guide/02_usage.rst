=====
Usage
=====

Basic Usage
===========

Create a new file ``mypodcasts.py``:

.. code-block:: python
    :linenos:

    #! /usr/bin/env python3

    import getpodcast

    opt = getpodcast.options(
        user_agent="Mozilla/5.0",
        date_from='2022-01-01',
        root_dir='./podcast')

    podcasts = {
        "SGU": "https://feed.theskepticsguide.org/feed/sgu",
        "Radiolab": "http://feeds.wnyc.org/radiolab"
    }

    getpodcast.getpodcast(podcasts, opt)

Mark ``mypodcasts.py`` as executable:

.. code-block:: console

    $ chmod a+x mypodcasts.py

Now you can download the ``SGU`` and ``Radiolab`` podcasts by typing:

.. code-block:: console

    $ ./mypodcasts.py --run

In this case, the run command will:

* Download all the ``SGU`` and ``Radiolab`` episodes since 2022
* Store the episodes in the ``./podcast`` folder. The folder will
  be created if it does not already exist.

If you run the same command again: 

* The file size provided in rss-feed and downloaded file is compared for
  every episode since 2022. New entries will be downloaded.
  Incomplete downloads will be resumed.

You can speed up this command by only checking the latest entries in rss-feed:

.. code-block:: console

    $ ./mypodcasts.py --run --onlynew

* Download the newest ``SGU`` and ``Radiolab`` episodes until a downloaded
  episode is found. 

You can override the `options <getpodcast.Options>` in ``mypodcast.py``
with command line arguments:

.. code-block:: console

    $ ./mypodcasts.py --run --date-from 2021-01-01 --root-dir ./episodes

.. note::
    
    Command line arguments takes precedence over options in ``mypodcasts.py``


Command line reference
======================

-h, --help

    Print help message and exit

    .. code-block:: text

        $ ./mypodcasts.py --help
        usage: mypodcasts.py [-h] [--run] [--dryrun] [--onlynew] [--deleteold] [--quiet][--list]
                        [--keywords] [--template TEMPLATE] [--podcast PODCAST] [--date-from DATE_FROM]
                        [--root-dir ROOT_DIR] [--user-agent USER_AGENT] [--hooks HOOKS]

        Download podcasts.

        optional arguments:
        -h, --help            show this help message and exit
        --run                 Download podcasts. (default: False)
        --dryrun              Test. Does not download podcasts. (default: False)
        --onlynew             Only process new entries. (default: False)
        --deleteold           Delete entries older than 'date_from' date. (default: False)
        --quiet               Quiet mode (for cron jobs) (default: False)
        --list                List all podcasts (default: False)
        --keywords            List available keywords for filename template (default: False)
        --template TEMPLATE   Filename template (default: {rootdir}/{podcast}/{year}/{date} {title}{ext})
        --podcast PODCAST     Only process this podcast. default: all (default: )
        --date-from DATE_FROM
                                Only download podcast newer then date (default: 2021-01-01)
        --root-dir ROOT_DIR   Podcast download directory (default: ./podcast)
        --user-agent USER_AGENT
                                web request identification (default: )
        --hooks HOOKS         comma separated key=value list of hooks (default: )

--run
    Download podcasts

--dryrun
    Show what would be done, without making any changes.

--onlynew
    ``default: False``
    Only process new entries.

--deleteold
    ``default: False``
    Delete entries older than 'date_from' date.

--quiet
    Quiet mode (for cron jobs)

--list
    List all podcasts

--keywords
    List available keywords for filename template

    .. code-block:: text

        {rootdir} = base directory for downloads
        {podcast} = key part of podcast dict
        {date} = item publish date (format: YYYY.MM.DD)
        {isodate} = item publish date (format: YYYY-MM-DD)
        {title} = item title
        {year} = item publish year (format: YYYY)
        {month} = item publish month (format: MM)
        {day} = item publish day (format: DD)
        {ext} = enclosure file extension
        {guid} = GUID of the episode

--template TEMPLATE
    ``default: {rootdir}/{podcast}/{year}/{date} {title}{ext}``
    Filename template
    
--podcast PODCAST
    ``default: all``
    Only process this podcast.

--date-from DATE_FROM
    ``default: 1970-01-01``
    Only download podcast newer then date

--root-dir ROOT_DIR
    ``default: ./podcast``
    Podcast download directory

--user-agent USER_AGENT
    ``default:``

    Web request identification.

    .. code-block:: console
        :caption: Example
        
        $ ./mypodcast.py --run --user-agent Mozilla/5.0

--hooks HOOKS
    ``default:``

    Comma separated ``key=value`` list of hooks
