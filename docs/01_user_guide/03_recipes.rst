=======
Recipes
=======

Basic example
=============

.. include:: ../../example1.py
    :start-after: """
    :end-before: """

.. literalinclude:: ../../example1.py
    :linenos:
    :start-at: import

Simple customizations
=====================

.. include:: ../../example2.py
    :start-after: """
    :end-before: """

.. literalinclude:: ../../example2.py
    :linenos:
    :start-at: import

Hooks
=====

.. include:: ../../example3.py
    :start-after: """
    :end-before: """

.. literalinclude:: ../../example3.py
    :linenos:
    :start-at: import

Custom date format
==================

.. include:: ../../example4.py
    :start-after: """
    :end-before: """

.. literalinclude:: ../../example4.py
    :linenos:
    :start-at: import

Automatic deletion
==================

.. include:: ../../example5.py
    :start-after: """
    :end-before: """

.. literalinclude:: ../../example5.py
    :linenos:
    :start-at: import
