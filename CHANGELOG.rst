Changelog
=========

Version 1.0.9 *(2022-08-20)*
----------------------------

* Added a ``deleteold`` option.
  (Ref `!1 <https://gitlab.com/fholmer/getpodcast/-/merge_requests/1>`_)

Version 1.0.8 *(2022-04-30)*
----------------------------

* Fix: Date format not as stated.
  (Ref `#1 <https://gitlab.com/fholmer/getpodcast/-/issues/1>`_)

Version 1.0.7 *(2021-01-12)*
----------------------------

* Skip podcast item if no audio
* Moved to Gitlab

Version 1.0.6.post1 *(2019-08-02)*
----------------------------------

* Fixed bug in `~getpodcast.parse_hooks_string`

Version 1.0.6 *(2019-08-02)*
----------------------------

* Added hooks.
* Added User-Agent header.
* Renamed `parseArguments_AsOptions` to `~getpodcast.options`

Version 1.0.5 *(2016-12-11)*
----------------------------

* Fix non-empty format string.
* Skip ``enclosure_length`` validation if ``enclosure_length`` is `None`.
* Changed allowed filename chars

Version 1.0.4 *(2016-08-13)*
----------------------------

* Retry download on timeout

Version 1.0.3 *(2016-08-07)*
----------------------------

* First public release